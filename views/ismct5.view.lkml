view: ismct5 {
  sql_table_name: "CLIENT_ANALYTICS"."ISMCT5"
    ;;

  dimension: analystemail {
    type: string
    sql: ${TABLE}."ANALYSTEMAIL" ;;
    label: "Analyst Email"
  }

  dimension: artistid {
    type: number
    value_format_name: id
    sql: ${TABLE}."ARTISTID" ;;
    label: "Artist ID"
  }

  dimension: businesspriority {
    type: string
    sql: ${TABLE}."BUSINESSPRIORITY" ;;
    label: "Business Priority"

  }

  dimension: citycountry {
    type: string
    sql: ${TABLE}."CITYCOUNTRY" ;;
    label: "City Country"
  }

  dimension: configid {
    type: string
    sql: ${TABLE}."CONFIGID" ;;
    label: "Config ID"
  }

  dimension: createdon {
    type: string
    sql: ${TABLE}."CREATEDON" ;;
    label: "Created On"
  }

  dimension: currentstep {
    type: string
    sql: ${TABLE}."CURRENTSTEP" ;;
    label: "Current Step"
  }

  dimension: discoveryvenueid {
    type: string
    sql: ${TABLE}."DISCOVERYVENUEID" ;;
    label: "Discovery Venue ID"
  }

  dimension: earliestpresale {
    type: string
    sql: ${TABLE}."EARLIESTPRESALE" ;;
    label: "Earliest Presale"
  }

  dimension: event_code {
    type: number
    sql: ${TABLE}."Event Code" ;;
    label: "Event Code"
  }

  dimension: eventid {
    type: number
    value_format_name: id
    sql: ${TABLE}."EVENTID" ;;
    label: "Event Code"
  }

  dimension: host {
    type: string
    sql: ${TABLE}."HOST" ;;
    label: "Host"
  }

  dimension: lastupdate {
    type: string
    sql: ${TABLE}."LASTUPDATE" ;;
    label: "Last Update"
  }

  dimension: mapanalyst {
    type: string
    sql: ${TABLE}."MAPANALYST" ;;
    label: "Map Analyst"
  }

  dimension: mfxsystem {
    type: string
    sql: ${TABLE}."MFXSYSTEM" ;;
    label: "Mfx System"
  }

  dimension: project {
    type: string
    sql: ${TABLE}."PROJECT" ;;
    label: "Project"
  }

  dimension: requestoremail {
    type: string
    sql: ${TABLE}."REQUESTOREMAIL" ;;
    label: "Requestor Email"
  }

  dimension: requestorname {
    type: string
    sql: ${TABLE}."REQUESTORNAME" ;;
    label: "Requestor Name"
  }

  dimension: requestsize {
    type: string
    sql: ${TABLE}."REQUESTSIZE" ;;
    label: "Request Size"
  }

  dimension: requesttype {
    type: string
    sql: ${TABLE}."REQUESTTYPE" ;;
    label: "Request Type"
  }

  dimension: seatingconfiguration {
    type: string
    sql: ${TABLE}."SEATINGCONFIGURATION" ;;
    label: "Seating Configuration"
  }

  dimension: status {
    type: string
    sql: ${TABLE}."STATUS" ;;
    label: "Status"
  }

  dimension: stmpism {
    type: string
    sql: ${TABLE}."STMP/ISM" ;;
    label: "STMp-ISM"
  }

  dimension: stmpname {
    type: string
    sql: ${TABLE}."STMPNAME" ;;
    label: "Stmp Name"
  }

  dimension: submission {
    type: string
    sql: ${TABLE}."SUBMISSION" ;;
    label: "Submission"
  }

  dimension: submission_name {
    type: string
    sql: ${TABLE}."SUBMISSION_NAME" ;;
    label: "Submission Name"
  }

  dimension: submissionduedate {
    type: number
    sql: ${TABLE}."SUBMISSIONDUEDATE" ;;
    label: "Submission Due Date"
  }

  dimension: submissions {
    type: number
    sql: ${TABLE}."SUBMISSIONS" ;;
    label: "Submissions"
  }

  dimension: tracker {
    type: string
    sql: ${TABLE}."TRACKER" ;;
    label: "Tracker"
  }

  dimension: venuefolder {
    type: string
    sql: ${TABLE}."VENUEFOLDER" ;;
    label: "Venue Folder"
  }

  dimension: venueid {
    type: number
    value_format_name: id
    sql: ${TABLE}."VENUEID" ;;
    label: "Venue ID"
  }

  dimension: venuename {
    type: string
    sql: ${TABLE}."VENUENAME" ;;
    label: "Venue Name"
  }

  measure: count {
    type: count
    drill_fields: [venuename, requestorname, submission_name, stmpname]
  }
}
