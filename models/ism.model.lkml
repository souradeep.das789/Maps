connection: "snowflake"

# include all the views
include: "/views/**/*.view"

datagroup: ism_default_datagroup {
  # sql_trigger: SELECT MAX(id) FROM etl_log;;
  max_cache_age: "1 hour"
}

persist_with: ism_default_datagroup

explore: ismct5 {}

# explore: arch_inventory {}

# explore: arch_inventory_20200420 {}

# explore: arch_inventory_20200422 {}

# explore: arch_inventory_20200428 {}

# explore: arch_inventory_20200511 {}

# explore: arch_inventory_20200715 {}

# explore: arch_inventory_3_day_wrap {}

# explore: arch_inventory_agg {}

# explore: arch_inventory_agg_daily {}

# explore: arch_inventory_agg_hourly {}

# explore: arch_inventory_daily {}

# explore: arch_inventory_detail {}

# explore: arch_inventory_lifetime {}

# explore: arch_inventory_new {}

# explore: arch_inventory_old {}

# explore: arch_inventory_returns {}

# explore: arch_inventory_returns_20200611 {}

# explore: arch_inventory_returns_20200714 {}

# explore: arch_inventory_returns_20200715 {}

# explore: arch_inventory_returns_new {}

# explore: arch_inventory_todays_sales {}

# explore: attendance_scan_event_vw1 {}

# explore: bi_dim_event {}

# explore: bi_refunds_credits_primary {}

# explore: bi_sec_event_tcodes {}

# explore: customer_order_buyer {}

# explore: entitlement {}

# explore: event_audit_archtics_only {}

# explore: event_tcode {}

# explore: event_time_zone {}

# explore: inventory_seat {}

# explore: inventory_seat_20200116 {}

# explore: inventory_seat_offer_set {}

# explore: inventory_seat_offer_set_20200116 {}

# explore: inventory_seat_offer_set_v1_20200618 {}

# explore: inventory_seat_v1_20200618 {}

# explore: jujamcyn {}

# explore: nederlander {}

# explore: nederlander_jujamcyn {}

# explore: order_activity_detail {}

# explore: order_detail {}

# explore: order_item_ticket {}

# explore: presence_scan_ods {}

# explore: presence_scan_source {}

# explore: presence_secure_render_ticket {}

# explore: refunds_credits_primary {}

# explore: rendered_tokens_backfill {}

# explore: rendered_tokens_invalidated_backfill {}

# explore: sales_comparison {}

# explore: sales_comparison_archtics_only {}

# explore: sales_comparison_archtics_only_20200803 {}

# explore: sales_comparison_archtics_only_20200812 {}

# explore: sales_comparison_archtics_only_new {}

# explore: sales_comparison_archtics_only_returns {}

# explore: sales_comparison_archtics_only_returns_20200714 {}

# explore: sales_comparison_archtics_only_returns_20200803 {}

# explore: sales_comparison_archtics_only_returns_20200812 {}

# explore: sales_comparison_archtics_only_returns_new {}

# explore: sales_comparison_combined {}

# explore: sales_comparison_host_only {}

# explore: sales_comparison_returns {}

# explore: sales_comparison_test {}

# explore: secure_render_ticket {}

# explore: t_amb_config_kafka {}

# explore: t_arena {}

# explore: t_defaults {}

# explore: t_event {}

# explore: t_ml_cust_demographics {}

# explore: t_ml_order_details {}

# explore: t_season {}

# explore: t_ticket {}

# explore: t_ticket_barcode {}

# explore: ticketweb_audit {}

